from setuptools import setup

setup(name='aniket',
      version='0.1',
      description='demo package',
      url='https://bitbucket.org/ANIKETANVARIA/aniket/src/master/',
      author='Aniket Anvaria',
      author_email='anvariaaniket@gmail.com',
      license='MIT',
      packages=['aniket'],
      zip_safe=False)